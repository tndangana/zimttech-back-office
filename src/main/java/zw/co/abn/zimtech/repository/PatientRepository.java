package zw.co.abn.zimtech.repository;

import org.springframework.stereotype.Repository;
import zw.co.abn.zimtech.model.Gender;
import zw.co.abn.zimtech.model.Patient;

import java.util.List;
public interface PatientRepository  extends GenericRepository<Patient>{
    List<Patient> findPatientsByAgeAfter(int age);
    List<Patient> findPatientsByGender(Gender gender);

    List<Patient> findPatientByAgeAfter(Integer age);

}
