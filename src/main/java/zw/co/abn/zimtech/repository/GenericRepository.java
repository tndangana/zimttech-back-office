package zw.co.abn.zimtech.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import zw.co.abn.zimtech.model.Base;

public interface GenericRepository<T extends Base> extends MongoRepository<T,String> {
}
