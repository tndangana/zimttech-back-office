package zw.co.abn.zimtech.repository;

import org.springframework.data.mongodb.repository.Query;
import zw.co.abn.zimtech.model.DiabeticScreening;

import java.time.LocalDate;
import java.util.List;

public interface DiabeticScreeningRepository extends GenericRepository<DiabeticScreening> {

    List<DiabeticScreening> getDiabeticScreeningByBloodGlucoseAfter(Float bloodGlucoseLevel);

    List<DiabeticScreening> getDiabeticScreeningsByBloodGlucoseBefore(Float bloodGlucoseLevel);

    List<DiabeticScreening> getDiabeticScreeningsByBloodPressureAfter(Float bloodPressure);

    List<DiabeticScreening> getDiabeticScreeningsByBloodPressureBefore(Float bloodPressure);

    List<DiabeticScreening> findDiabeticScreeningsByTreatmentStartDateAfterOrTreatmentStartDateEquals(LocalDate date);

    List<DiabeticScreening> findDiabeticScreeningsByPatientId(String patientId);


    @Query(value = "{$and: ["
            + "?#{ [0] == null ? {_id: {$ne: null}} : {weight: {$gte: [0]}}} ,"
            + "?#{ [1] == null ? {_id: {$ne: null}} : {weight: {$lte: [1]}}} ,"
            + "?#{ [2] == null ? {_id: {$ne: null}} : {height: {$gte: [2]}}} ,"
            + "?#{ [3] == null ? {_id: {$ne: null}} : {height: {$lte: [3]}}} ,"
            + "?#{ [4] == null ? {_id: {$ne: null}} : {bloodPressure: {$gte: [4]}}} ,"
            + "?#{ [5] == null ? {_id: {$ne: null}} : {bloodPressure: {$lte: [5]}}} ,"
            + "?#{ [6] == null ? {_id: {$ne: null}} : {bloodGlucose: {$gte: [6]}}} ,"
            + "?#{ [7] == null ? {_id: {$ne: null}} : {bloodGlucose: {$lte: [7]}}} ,"
            + "?#{ [8] == null ? {_id: {$ne: null}} : {treatmentStartDate: {$lte: [8]}}}"
            + "]}", fields = "{_id: 0}")
    List<DiabeticScreening> search(Float weightMin, Float weightMax, Float heightMin, Float heightMax, Float bloodPressureMin, Float bloodPressureMax, Float bloodGlucoseMin, Float bloodGlucoseMax,LocalDate treatmentDate);
}

