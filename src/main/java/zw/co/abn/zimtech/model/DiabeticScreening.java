package zw.co.abn.zimtech.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Document(collection = "diabetic_screening")
@Data
@EqualsAndHashCode
public class DiabeticScreening extends Base {
    private String patientId;
    private Float weight;
    private Float height;
    private Float bloodPressure;
    private Float bloodGlucose;
    private LocalDate treatmentStartDate = LocalDate.now();
    private LocalDate treatmentDate = LocalDate.now();
}
