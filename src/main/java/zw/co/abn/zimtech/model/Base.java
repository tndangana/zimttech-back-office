package zw.co.abn.zimtech.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;

@Data
public abstract class Base {
    @ApiModelProperty(readOnly = true)
    @Id
    private String id;
    private LocalDate createdDate = LocalDate.now();
    private LocalDate updatedDate = LocalDate.now();
}
