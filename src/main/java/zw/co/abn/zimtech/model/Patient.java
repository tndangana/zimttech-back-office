package zw.co.abn.zimtech.model;

import lombok.Data;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data@Document(collection = "patient")
public class Patient  extends Base{
    private String firstName;
    private String lastName;
    private Gender gender;
    private Integer age;
    private String mobileNumber;

}
