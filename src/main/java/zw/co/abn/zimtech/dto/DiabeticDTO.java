package zw.co.abn.zimtech.dto;

import lombok.Data;
import org.springframework.data.annotation.Id;
import zw.co.abn.zimtech.model.Patient;

import java.time.LocalDate;

@Data
public class DiabeticDTO {
    private String id;
    private Patient patient;
    private Float weight;
    private Float height;
    private Float bloodPressure;
    private Float bloodGlucose;
    private LocalDate treatmentStartDate = LocalDate.now();
}
