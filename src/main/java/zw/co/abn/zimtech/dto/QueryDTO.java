package zw.co.abn.zimtech.dto;

import lombok.Data;
import org.springframework.web.bind.annotation.RequestParam;

@Data
public class QueryDTO {
    private Float weightMin;
    private Float weightMax;
    private Float heightMin;
    private Float heightMax;
    private Float bloodPressureMin;
    private Float bloodPressureMax;
    private Float bloodGlucoseMin;
    private Float bloodGlucoseMax;
}
