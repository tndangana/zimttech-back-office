package zw.co.abn.zimtech.dto;

import lombok.Data;
import org.springframework.data.annotation.Transient;
import zw.co.abn.zimtech.model.DiabeticScreening;
import zw.co.abn.zimtech.model.Gender;

import java.util.List;
@Data
public class PatientDTO {
    private String id;
    private String firstName;
    private String lastName;
    private Gender gender;
    private Integer age;
    private String mobileNumber;
    List<DiabeticScreening> diabeticScreeningList;
}
