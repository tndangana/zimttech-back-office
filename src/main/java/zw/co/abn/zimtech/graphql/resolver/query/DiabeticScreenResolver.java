package zw.co.abn.zimtech.graphql.resolver.query;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.stereotype.Component;
import zw.co.abn.zimtech.model.DiabeticScreening;
import zw.co.abn.zimtech.model.Patient;
import zw.co.abn.zimtech.service.PatientService;

@Component
public class DiabeticScreenResolver implements GraphQLResolver<DiabeticScreening> {

    private PatientService patientService;

    public DiabeticScreenResolver(PatientService patientService) {
        this.patientService = patientService;
    }

    public Patient patient(DiabeticScreening diabeticScreening){
        return patientService.findById(diabeticScreening.getPatientId()).orElse(null);
    }
}
