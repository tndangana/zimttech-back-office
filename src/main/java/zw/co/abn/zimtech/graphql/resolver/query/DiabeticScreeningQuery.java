package zw.co.abn.zimtech.graphql.resolver.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
//import io.leangen.graphql.annotations.GraphQLQuery;
import org.springframework.stereotype.Component;
import zw.co.abn.zimtech.model.DiabeticScreening;
import zw.co.abn.zimtech.service.DiabeticService;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
public class DiabeticScreeningQuery implements GraphQLQueryResolver {

    private DiabeticService diabeticService;

    public DiabeticScreeningQuery(DiabeticService diabeticService) {
        this.diabeticService = diabeticService;
    }

    public Optional<List<DiabeticScreening>> diabeticScreeningList() {
        return Optional.ofNullable(diabeticService.findAll()).get();
    }

    public DiabeticScreening findDiabeticScreeningById(String id) {
        Optional<DiabeticScreening> diabeticScreening = diabeticService.findById(id);
        return diabeticScreening.get();
    }

    public List<DiabeticScreening> getListByBloodPressureAfter(Float bloodPressure) {
        List<DiabeticScreening> diabeticScreeningList = diabeticService.getListByBloodPressureLevelAfter(bloodPressure);
        if (diabeticScreeningList.isEmpty()) {
            return null;
        }
        return diabeticScreeningList;
    }

    public List<DiabeticScreening> getListByBloodPressureBefore(Float bloodPressure) {
        List<DiabeticScreening> diabeticScreeningList = diabeticService.getListByBloodPressureLevelBefore(bloodPressure);
        if (diabeticScreeningList.isEmpty()) {
            return null;
        }
        return diabeticScreeningList;
    }

    public List<DiabeticScreening> getListByBloodGlucoseBefore(Float bloodGlucose) {
        List<DiabeticScreening> diabeticScreeningList = diabeticService.getListByBloodGlucoseLevelBefore(bloodGlucose);
        if (diabeticScreeningList.isEmpty()) {
            return null;
        }
        return diabeticScreeningList;
    }

    public List<DiabeticScreening> getListByBloodGlucoseAfter(Float bloodGlucose) {
        List<DiabeticScreening> diabeticScreeningList = diabeticService.getListByBloodGlucoseLevelAfter(bloodGlucose);
        if (diabeticScreeningList.isEmpty()) {
            return null;
        }
        return diabeticScreeningList;
    }
}
