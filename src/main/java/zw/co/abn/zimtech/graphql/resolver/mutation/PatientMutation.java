package zw.co.abn.zimtech.graphql.resolver.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.stereotype.Component;
import zw.co.abn.zimtech.model.Patient;
import zw.co.abn.zimtech.service.PatientService;

import java.util.Optional;

@Component
public class PatientMutation implements GraphQLMutationResolver {
    private PatientService patientService;

    public PatientMutation(PatientService patientService) {
        this.patientService = patientService;
    }

    public Patient createPatient(Patient patient) {
        return patientService.save(patient);
    }

    public Patient updatePatient(Patient patient, String id) {
         return patientService.update(patient,id).get();
    }

    public Boolean deletePatientById(Optional<String> id) {
        id.ifPresent(this::accept);
        return true;
    }

    private void accept(String id) {
        Optional<Patient> patient = patientService.findById(id);
        patientService.deleteById(patient.get().getId());
    }
}
