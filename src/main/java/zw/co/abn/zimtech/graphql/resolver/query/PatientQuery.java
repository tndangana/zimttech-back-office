package zw.co.abn.zimtech.graphql.resolver.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.stereotype.Component;
import zw.co.abn.zimtech.model.Patient;
import zw.co.abn.zimtech.service.PatientService;

import java.util.List;
import java.util.Optional;

@Component
public class PatientQuery  implements GraphQLQueryResolver {

    private  final PatientService patientService;

    public PatientQuery(PatientService patientService) {
        this.patientService = patientService;
    }

    public Optional<List<Patient>> patientList(){
         return Optional.of(patientService.findAll().get());
    }

    public Patient patientById(String id){
        return patientService.findById(id).get();
    }
}
