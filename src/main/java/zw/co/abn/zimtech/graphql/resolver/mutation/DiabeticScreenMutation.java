package zw.co.abn.zimtech.graphql.resolver.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.stereotype.Component;
import zw.co.abn.zimtech.model.DiabeticScreening;
import zw.co.abn.zimtech.service.DiabeticService;

import java.util.Optional;

@Component
public class DiabeticScreenMutation implements GraphQLMutationResolver {

    private DiabeticService diabeticService;

    public DiabeticScreenMutation(DiabeticService diabeticService) {
        this.diabeticService = diabeticService;
    }

    public DiabeticScreening createDiabeticScreening(DiabeticScreening diabeticScreening) {
        return diabeticService.save(diabeticScreening);
    }

    public DiabeticScreening updateDiabeticScreening(DiabeticScreening diabeticScreening, String id) {
        return diabeticService.update(diabeticScreening, id).get();
    }

    public Boolean deleteScreenById(Optional<String> id) {
        id.ifPresent(this::accept);
        return true;
    }

    private void accept(String id) {
        Optional<DiabeticScreening> order = diabeticService.findById(id);
        diabeticService.deleteById(order.get().getId());
    }
}
