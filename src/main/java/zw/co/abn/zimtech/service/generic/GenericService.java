package zw.co.abn.zimtech.service.generic;

import org.springframework.beans.BeanUtils;
import zw.co.abn.zimtech.model.Base;
import zw.co.abn.zimtech.repository.GenericRepository;
import zw.co.abn.zimtech.service.IGenericService;

import java.util.List;
import java.util.Optional;

public abstract class GenericService<T extends Base, ID extends String> implements IGenericService<T, ID> {

    private final GenericRepository<T> genericRepository;

    public GenericService(GenericRepository<T> genericRepository) {
        this.genericRepository = genericRepository;
    }

    @Override
    public T save(T t) {
        return genericRepository.save(t);
    }

    @Override
    public void deleteById(ID s) {
        genericRepository.deleteById(s);
    }

    @Override
    public Optional<T> findById(ID s) {
        return Optional.ofNullable(genericRepository.findById(s).get());
    }

    @Override
    public Optional<List<T>> findAll() {
        return Optional.ofNullable(genericRepository.findAll());
    }

    @Override
    public Optional<T> update(T t, ID s) {
        return genericRepository.findById(s).map(existingT -> {
            BeanUtils.copyProperties(t, existingT);
            return genericRepository.save(existingT);
        });

    }


}
