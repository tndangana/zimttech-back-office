package zw.co.abn.zimtech.service;

import org.springframework.stereotype.Service;
import zw.co.abn.zimtech.dto.DiabeticDTO;
import zw.co.abn.zimtech.model.DiabeticScreening;
import zw.co.abn.zimtech.model.Patient;
import zw.co.abn.zimtech.repository.DiabeticScreeningRepository;
import zw.co.abn.zimtech.repository.GenericRepository;
import zw.co.abn.zimtech.repository.PatientRepository;
import zw.co.abn.zimtech.service.generic.GenericService;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DiabeticService extends GenericService<DiabeticScreening, String> {
    private DiabeticScreeningRepository diabeticScreeningRepository;
    private PatientRepository patientRepository;

    public DiabeticService(GenericRepository<DiabeticScreening> genericRepository,
                           DiabeticScreeningRepository diabeticScreeningRepository, PatientRepository patientRepository) {
        super(genericRepository);
        this.diabeticScreeningRepository = diabeticScreeningRepository;
        this.patientRepository = patientRepository;
    }

    public List<DiabeticScreening> findListByPatientId(String id) {
        return Optional.of(diabeticScreeningRepository.findDiabeticScreeningsByPatientId(id)).get();
    }

    public List<DiabeticScreening> getListByBloodPressureLevelAfter(Float bloodPressure) {
        return Optional.of(diabeticScreeningRepository.getDiabeticScreeningsByBloodPressureAfter(bloodPressure)).get();
    }

    public List<DiabeticScreening> getListByBloodPressureLevelBefore(Float bloodPressure) {
        return Optional.of(diabeticScreeningRepository.getDiabeticScreeningsByBloodPressureBefore(bloodPressure)).get();
    }

    public List<DiabeticScreening> getListByBloodGlucoseLevelBefore(Float bloodGlucose) {
        return Optional.of(diabeticScreeningRepository.getDiabeticScreeningsByBloodGlucoseBefore(bloodGlucose)).get();
    }

    public List<DiabeticScreening> getListByBloodGlucoseLevelAfter(Float bloodGlucose) {
        return Optional.of(diabeticScreeningRepository.getDiabeticScreeningByBloodGlucoseAfter(bloodGlucose)).get();
    }

    public List<DiabeticScreening> findDiabeticScreeningsByTreatmentStartDateAfter(LocalDate treatmentStartDate) {
        return Optional.of(diabeticScreeningRepository.findDiabeticScreeningsByTreatmentStartDateAfterOrTreatmentStartDateEquals(treatmentStartDate)).get();
    }


    // Search is for patients above 18 and have been on treatment for the past 18months
    public List<DiabeticDTO> search(Float weightMin, Float weightMax, Float heightMin, Float heightMax, Float bloodPressureMin, Float bloodPressureMax, Float bloodGlucoseMin, Float bloodGlucoseMax, LocalDate treatmentDate) {
        List<DiabeticDTO> diabeticDTOList = new ArrayList<>();
        List<DiabeticScreening> diabeticScreeningList = diabeticScreeningRepository.search(weightMin, weightMax, heightMin, heightMax, bloodPressureMin, bloodPressureMax, bloodGlucoseMin, bloodGlucoseMax, treatmentDate);
        if (!diabeticScreeningList.isEmpty()) {
            for (int i = 0; i < diabeticScreeningList.size(); i++) {
                Patient patient = patientRepository.findById(diabeticScreeningList.get(i).getPatientId()).get();

                if(patient.getAge() >= 18){
                    DiabeticDTO diabeticDTO = new DiabeticDTO();
                    diabeticDTO.setId(diabeticScreeningList.get(i).getId());
                    diabeticDTO.setPatient(patient);
                    diabeticDTO.setHeight(diabeticScreeningList.get(i).getHeight());
                    diabeticDTO.setWeight(diabeticScreeningList.get(i).getWeight());
                    diabeticDTO.setBloodGlucose(diabeticScreeningList.get(i).getBloodGlucose());
                    diabeticDTO.setBloodPressure(diabeticScreeningList.get(i).getBloodPressure());
                    diabeticDTO.setTreatmentStartDate(diabeticScreeningList.get(i).getTreatmentStartDate());
                    diabeticDTOList.add(diabeticDTO);
                }

            }
            return diabeticDTOList;
        }
        return null;
    }
}
