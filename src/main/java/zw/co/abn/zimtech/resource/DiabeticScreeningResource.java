package zw.co.abn.zimtech.resource;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import zw.co.abn.zimtech.dto.DiabeticDTO;
import zw.co.abn.zimtech.dto.QueryDTO;
import zw.co.abn.zimtech.model.DiabeticScreening;
import zw.co.abn.zimtech.resource.generic.GenericResource;
import zw.co.abn.zimtech.service.DiabeticService;
import zw.co.abn.zimtech.service.generic.GenericService;

import java.time.LocalDate;
import java.util.List;


@RestController
@RequestMapping("/api/screening")
@CrossOrigin(origins = "*", maxAge = 3600)
public class DiabeticScreeningResource extends GenericResource<DiabeticScreening, String> {
    private DiabeticService diabeticService;

    public DiabeticScreeningResource(GenericService<DiabeticScreening, String> service, DiabeticService diabeticService) {
        super(service);
        this.diabeticService = diabeticService;
    }

    @Override
    @PostMapping("/")
    public ResponseEntity<DiabeticScreening> create(@RequestBody @Validated DiabeticScreening diabeticScreening) {
        List<DiabeticScreening> diabeticScreeningList = diabeticService.findListByPatientId(diabeticScreening.getPatientId());
        if (diabeticScreeningList.isEmpty()) {
            // Patient has not started treatment
            diabeticScreening.setTreatmentStartDate(diabeticScreening.getTreatmentDate());
            return new ResponseEntity<>(diabeticService.save(diabeticScreening), HttpStatus.CREATED);
        }
        //Patient already started treatment
        diabeticScreening.setTreatmentStartDate(diabeticScreeningList.get(0).getTreatmentStartDate());
        return new ResponseEntity<>(diabeticService.save(diabeticScreening), HttpStatus.CREATED);

    }

    @GetMapping("/{patientId}")
    public ResponseEntity<List<DiabeticScreening>> getListByPatientId(@PathVariable("patientId") String patientId) {
        List<DiabeticScreening> diabeticScreeningList = diabeticService.findListByPatientId(patientId);
        if (diabeticScreeningList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(diabeticScreeningList, HttpStatus.OK);
    }

    @GetMapping("/treatment-date")
    public ResponseEntity<List<DiabeticScreening>> getListByTreatmentDateAfter(@RequestParam(name = "treatmentStartDate") LocalDate treatmentStartDate) {
        List<DiabeticScreening> diabeticScreeningList = diabeticService.findDiabeticScreeningsByTreatmentStartDateAfter(treatmentStartDate);
        if (diabeticScreeningList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(diabeticScreeningList, HttpStatus.OK);
    }

    @GetMapping("/blood-pressure-after")
    public ResponseEntity<List<DiabeticScreening>> getListByBloodPressureAfter(@RequestParam(name = "bloodPressure", required = true) Float bloodPressure) {
        List<DiabeticScreening> diabeticScreeningList = diabeticService.getListByBloodPressureLevelAfter(bloodPressure);
        if (diabeticScreeningList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(diabeticScreeningList, HttpStatus.OK);
    }

    @GetMapping("/blood-pressure-before")
    public ResponseEntity<List<DiabeticScreening>> getListByBloodPressureBefore(@RequestParam(name = "bloodPressure", required = true) Float bloodPressure) {
        List<DiabeticScreening> diabeticScreeningList = diabeticService.getListByBloodPressureLevelBefore(bloodPressure);
        if (diabeticScreeningList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(diabeticScreeningList, HttpStatus.OK);
    }

    @GetMapping("/blood-glucose-before")
    public ResponseEntity<List<DiabeticScreening>> getListByBloodGlucoseBefore(@RequestParam(name = "bloodGlucose") Float bloodGlucose) {
        List<DiabeticScreening> diabeticScreeningList = diabeticService.getListByBloodGlucoseLevelBefore(bloodGlucose);
        if (diabeticScreeningList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(diabeticScreeningList, HttpStatus.OK);
    }

    @GetMapping("/blood-glucose-after")
    public ResponseEntity<List<DiabeticScreening>> getListByBloodGlucoseAfter(@RequestParam(name = "bloodGlucose") Float bloodGlucose) {
        List<DiabeticScreening> diabeticScreeningList = diabeticService.getListByBloodGlucoseLevelAfter(bloodGlucose);
        if (diabeticScreeningList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(diabeticScreeningList, HttpStatus.OK);
    }

    @PostMapping("/filter")
    public ResponseEntity<List<DiabeticDTO>> getFilteredDiagnosticResults(@RequestBody @Validated QueryDTO queryDTO) {
        System.out.println("weightMin :" + queryDTO);
        List<DiabeticDTO> diabeticScreeningList = diabeticService
                .search(queryDTO.getWeightMin(), queryDTO.getWeightMax(), queryDTO.getHeightMin(), queryDTO.getHeightMax(),
                        queryDTO.getBloodPressureMin(), queryDTO.getBloodPressureMax(), queryDTO.getBloodGlucoseMin(), queryDTO.getBloodGlucoseMax(), LocalDate.now().minusMonths(18));
        if (!diabeticScreeningList.isEmpty()) {
            System.out.println("diabeticScreeningList" + diabeticScreeningList.size());
            return new ResponseEntity<>(diabeticScreeningList, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
