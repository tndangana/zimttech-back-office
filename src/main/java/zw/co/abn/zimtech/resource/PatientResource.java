package zw.co.abn.zimtech.resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import zw.co.abn.zimtech.dto.PatientDTO;
import zw.co.abn.zimtech.model.Patient;
import zw.co.abn.zimtech.resource.generic.GenericResource;
import zw.co.abn.zimtech.service.PatientService;
import zw.co.abn.zimtech.service.generic.GenericService;

import java.util.List;

@RestController
@RequestMapping("/api/patient")
@CrossOrigin(origins = "*", maxAge = 3600)
public class PatientResource extends GenericResource<Patient,String> {
    private PatientService patientService;
    public PatientResource(GenericService<Patient, String> service,PatientService patientService) {
        super(service);
        this.patientService = patientService;
    }

    @GetMapping("/list")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<List<PatientDTO>> getPatientListWiScreen(){
        List<PatientDTO> patientDTOList = patientService.getPatientWithDiagnosticList();
        if (patientDTOList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(patientDTOList, HttpStatus.OK);
    }

    @GetMapping("/get-list-age")
    public ResponseEntity<List<PatientDTO>> getPatientListByAge(@RequestParam(name="age") Integer age){
        List<PatientDTO> patientDTOList = patientService.getPatientsByAgeAfter(age);
        if (!patientDTOList.isEmpty()) {
            return new ResponseEntity<>(patientDTOList, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @GetMapping("/patient-adult-18")
    public ResponseEntity<List<PatientDTO>> getPatients18(){
        List<PatientDTO> patientDTOList = patientService.getPatientsByAgeAfter18();
        if (!patientDTOList.isEmpty()) {
            return new ResponseEntity<>(patientDTOList, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
