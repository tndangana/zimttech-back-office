package zw.co.abn.zimtech.resource.generic;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import zw.co.abn.zimtech.model.Base;
import zw.co.abn.zimtech.service.generic.GenericService;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
public class GenericResource<T extends Base, ID extends String> implements IGenericResource<T, ID> {

    private final GenericService<T, ID> service;

    public GenericResource(GenericService<T, ID> service) {
        this.service = service;
    }

    @Override
    @PostMapping("/")
    public ResponseEntity<T> create(@RequestBody @Validated T t) {
        try {
            service.save(t);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @GetMapping("/")
    public ResponseEntity<List<T>> getAll() {
        try {
            System.out.println("-0-0-0-0-0-0-0-0-0-0-0");
            Optional<List<T>> list = service.findAll();
            return list.map(ts -> new ResponseEntity<>(ts, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity<T> deleteById(@PathVariable("id") ID id) {
        try {
            Optional<T> optionalT = Optional.of(service.findById(id).get());
            optionalT.ifPresent(t1 -> service.deleteById((ID) t1.getId()));
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @PutMapping("/{id}")
    public ResponseEntity<T> update(@RequestBody @Validated T t, @PathVariable("id") ID id) {
        try {
            T t1 = service.update(t, id).get();
            return new ResponseEntity<>(t1, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @Override
    @GetMapping("/{id}")
    public ResponseEntity<T> find(@PathVariable("id") ID id) {
        try {
            Optional<T> t = service.findById(id);
            return t.map(value -> new ResponseEntity<>(value, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
