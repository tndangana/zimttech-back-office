package zw.co.abn.zimtech.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Filter {
    private Object value;
    private boolean isLessThan;
}
