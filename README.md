# ZIMTTECH LEAD APPLICATION EXERCISE
# This project is running on Java 8 enviroment
# Inorder to run it install the following
1. oracle jdk 8
2. mongodb 4.4
3. gradle version 7.2

The mongo database auth should not be configured for the purpose of this exercise
When all this is done run application using the following url. It will serve the React application build
# http://localhost:9090/
To access swagger enter the following url 
# http://localhost:9090/swagger-ui/
To access graphql, enter the following url
# http://localhost:9090/graphql
# For the purpose of this exercise i have created a jar package where you can take existing jar and run it given that the above-mentioned requirements are adhered too